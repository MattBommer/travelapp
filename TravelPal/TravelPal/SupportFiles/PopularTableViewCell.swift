//
//  PopularTableViewCell.swift
//  TravelPal
//
//  Created by Matt Bommer on 10/21/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit

//Custom TableViewCell class to feature the contents of trips to users.
class PopularTableViewCell: UITableViewCell {
    
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var tripLocation: UILabel!
    @IBOutlet weak var tripDate: UILabel!
    @IBOutlet weak var headerPicture: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var tripName: UILabel!
    var trip:Trip!
    
    //Increments the value of the like count
    @IBAction func likeButtonPressed(_ sender: Any) {
        if !trip.liked {
            trip.incrementLike()
            self.numberOfLikes.text = "\(trip.likeCount) Likes"
            self.numberOfLikes.sizeToFit()
            likeButton.setImage(UIImage(systemName: "hand.thumbsup.fill"), for: .normal)
        }
    }
    
    //Puts the trip in the saved section.
    @IBAction func saveButtonPressed(_ sender: Any) {
        if !appUser.tripIds.contains(self.trip.uniqueId) {
            appUser.addTrip(trip: self.trip!, wasPopular: true)
        }
        saveButton.setImage(UIImage(systemName: "suit.heart.fill"), for: .normal)
    }
    
    //Sets the image of the tableview cell.
    func setImage(image: UIImage) {
        self.headerPicture.image = image 
    }
}
