//
//  ChatsTableViewCell.swift
//  TravelPal
//
//  Created by Sooyong on 10/22/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit

class BlockedUser: UITableViewCell {
    var blockedImage: UIImage!
    var blockedName = String()
    @IBOutlet weak var blockedImageView: UIImageView!
    @IBOutlet weak var blockedNameLabel: UILabel!
}
