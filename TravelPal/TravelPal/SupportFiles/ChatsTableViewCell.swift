//
//  ChatsTableViewCell.swift
//  TravelPal
//
//  Created by Eric on 10/22/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit

class Chats: UITableViewCell {
    var profileImage: UIImage!
    var profileName = String()
    var profileEmail = String()
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
}
