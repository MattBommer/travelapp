//
//  ChatProfileViewController.swift
//  TravelPal
//
//  Created by Eric Wu on 10/21/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit

class ChatProfileViewController: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var dreamDestinationLabel0: UILabel!
    @IBOutlet weak var dreamDestinationLabel1: UILabel!
    @IBOutlet weak var dreamDestinationLabel2: UILabel!
    @IBOutlet weak var blockButton: UIButton!
    //This button will be hidden if coming from chat, and itll show when
    //connected by see trips page
    @IBOutlet weak var gotoChatButton: UIBarButtonItem!
    
    var user: AppUser!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // All initial data will be from Firestore
        presentingViewController?.title = user.name
        nameLabel.text = user.name
        ageLabel.text = user.age
        locationLabel.text = user.location
        dreamDestinationLabel0.text = user.dest0
        dreamDestinationLabel1.text = user.dest1
        dreamDestinationLabel2.text = user.dest2
        bioLabel.text = user.bio
        let pathReference = storage.reference(withPath:"userImages/\(user.email).png")
        pathReference.getData(maxSize: 20 * 512 * 512) { data, error in
            if let error = error {
                print("An error occurred while trying to download the picture: \(error)")
                self.profileImage.image = UIImage(named: "defaultProfile")
            }
            else {
                self.profileImage.image = UIImage(data: data!)
            }
        }
    }
    
    @IBAction func blockUser(_ sender: Any) {
        appUser.blockedList.append(user.email)
        if let index = appUser.chatList.firstIndex(of: user.email) {
            appUser.chatList.remove(at: index)
        }
        db.collection("users").document(appUser.email).updateData([
            "chatList": appUser.chatList,
            "blockedList": appUser.blockedList
        ]) { err in
            if let err = err {
                print("Error writing document:\(err)")
            } else {
                print("Document successfuly written!")
            }
        }
        navigationController?.popViewController(animated: true)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func gotoChat(_ sender: Any) {
    }
}
