//
//  ChatsViewController.swift
//  TravelPal
//
//  Created by Eric on 10/22/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit
import FirebaseFirestore

class ChatsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var addChat: UIBarButtonItem!

    let textCellIdentifier = "ChatCell"
    let personChatSegueIdentifier = "PersonChatSegueIdentifier"
    var chatsList:[Chats] = []
    var clickedPerson: AppUser!
    var peopleChatsList:[AppUser] = []
    var viewFinishedLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chatTableView.delegate = self
        chatTableView.dataSource = self
        
        chatsList.removeAll()
        newChatListener()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
    }
    
    //Listener to wait for messages from new people, if there are new chats, update chatslist
    func newChatListener() {
        db.collection("users").document(appUser.email)
            .addSnapshotListener { [self] documentSnapshot, error in
              guard let document = documentSnapshot else {
                print("Error fetching document: \(error!)")
                return
              }
              guard let data = document.data() else {
                print("Document data was empty.")
                return
              }
                let tempChatsList: [String] = documentSnapshot?.get("chatList")! as? [String] ?? []
                if tempChatsList.count != chatsList.count {
                    chatsList.removeAll()
                    loadChatHistory()
                }
            }
    }
        
    //Load chat history from the database
    func loadChatHistory() {
        let docRef = db.collection("users").document(appUser.email).getDocument{ (document, error) in
            if let document = document, document.exists {
                appUser.chatList = document.data()!["chatList"] as? [String] ?? []
                let emailList = appUser.chatList
                if emailList.count == 0 {
                    self.chatTableView.reloadData()
                }
                for email in emailList {
                    let chatUser = Chats()
                    chatUser.profileEmail = email
                    let pathReference = storage.reference(withPath:"userImages/\(email).png")
                    pathReference.getData(maxSize: 20 * 512 * 512) { data, error in
                        if let error = error {
                            print("An error occurred while trying to download the picture: \(error)")
                            chatUser.profileImage = UIImage(named: "defaultProfile")
                            self.chatTableView.reloadData()
                        }
                        else {
                            chatUser.profileImage = UIImage(data: data!)
                            self.chatTableView.reloadData()
                        }
                    }
                    let docRef = db.collection("users").document(email)
                    docRef.getDocument { (document, error) in
                        if let document = document, document.exists {
                            let dataDescription = document.data()
                            chatUser.profileName = dataDescription!["name"] as? String ?? ""
                            print("chatUser.profileName",chatUser.profileName)
                            print("Chat User:", chatUser)
                            self.addToChatList(chatUser)
                        }
                    }
                    // Get the user's profile image from the storage db
                    let path = storage.reference(withPath: "userImages/\(email).png")
                        path.getData(maxSize: 20 * 512 * 512) { data, error in
                          if let error = error {
                            print("An error occured while trying to download the picture: \(error)")

                          } else {
                            print("Image was successfully retrieved from database")
                            chatUser.profileImage = UIImage(data: data!)!
                            self.chatTableView.reloadData()
                          }
                    }
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        chatsList.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath as IndexPath) as! Chats
        let row = indexPath.row
        print("Document Data NAME: ", chatsList[row].profileName)

        cell.profileImageView.image = chatsList[row].profileImage
        cell.profileNameLabel.text = chatsList[row].profileName

        
        return cell
    }
    
    //Pass the chatting user's info into the ChatViewController
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedIndex = self.chatTableView.indexPathForSelectedRow!.row
        let docRef = db.collection("users").document(chatsList[selectedIndex].profileEmail)
        docRef.getDocument { [self] (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data()
                print("Document clicking on chat ", dataDescription ?? "Nodata")
                let userData = document.data()
                clickedPerson = AppUser(userData!)
                self.performSegue(withIdentifier: self.personChatSegueIdentifier, sender: self)

            } else {
                //Throw an error or something if that user doesn't exist
                print("Chat User does not exist when clicked")
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == personChatSegueIdentifier,
           let nextVC = segue.destination as? ChatViewController {
                nextVC.user2 = clickedPerson
	        }
    }
    
    //Throw an alert for whenever the user adds a friend and adds the info the the database
    @IBAction func clickedAddChat(_ sender: Any) {
        let controller = UIAlertController(title: "Add Chat",
                                           message: "Enter Email",
                                           preferredStyle: .alert)
        
        controller.addAction(UIAlertAction(title: "Cancel",
                                           style: .cancel,
                                           handler: nil))
        
        controller.addTextField(configurationHandler: {
            (textField:UITextField!) in textField.placeholder = "Enter Something"
        })
        
        controller.addAction(UIAlertAction(title: "OK",
                                           style: .default,
                                           handler: { [self]
                                            (paramAction:UIAlertAction!) in
                                            if let textFieldArray = controller.textFields {
                                                let textFields = textFieldArray as [UITextField]
                                                let enteredText = textFields[0].text ?? ""
                                                // do something to show it worked
                                                updateAndAddChatListsHelper(controller, enteredText)
                                            }
                }))
        present(controller, animated: true, completion: nil)
    }
    
    //Helper function to shorten clickedAddChat. It adds the emails to a chatlist in the database so we know who we've talked to
    func updateAndAddChatListsHelper(_ controller: UIAlertController, _ enteredText: String) {
        if !appUser.blockedList.contains(enteredText) {
            if !chatsList.contains(where: { $0.profileEmail == enteredText }) {
                let docRef = db.collection("users").document(enteredText)
                docRef.getDocument { (document, error) in
                    if let document = document, document.exists {
                        let dataDescription = document.data()
                        print("Document Data Chats: ", dataDescription as Any)
                        let newChatUser = Chats()
                        newChatUser.profileName = dataDescription!["name"] as? String ?? ""
                        let pathReference = storage.reference(withPath: "userImages/\(enteredText).png")
                        pathReference.getData(maxSize: 20 * 512 * 512) { data, error in
                            if let error = error {
                                print("An error occurred while trying to download the picture: \(error)")
                                newChatUser.profileImage = UIImage(named: "defaultProfile")
                            }
                            else {
                                newChatUser.profileImage = UIImage(data: data!)
                            }
                            newChatUser.profileEmail = dataDescription!["email"] as? String ?? ""
                            appUser.chatList.append(dataDescription!["email"] as? String ?? "")
                            // Add the newChatUser to the chatlist
                            db.collection("users").document(appUser.email).updateData([
                                "chatList": appUser.chatList
                            ]) { err in
                                if let err = err {
                                    print("Error writing document: \(err)")
                                } else {
                                    print("Document successfuly written!")
                                }
                            }
                            // Go to newChatUser and add your email to their chatlist
                            var newUserChatList = dataDescription!["chatList"] as? [String] ?? []
                            newUserChatList.append(appUser.email)
                            docRef.updateData([
                                "chatList": newUserChatList
                            ]) { err in
                                if let err = err {
                                    print("Error writing New ChatUser document: \(err)")
                                } else {
                                    print("New ChatUser Document successfuly written!")
                                }
                            }
                            self.addToChatList(newChatUser)
                        }
                    } else {
                        //Throw an error or something if that user doesn't exist
                        print("Document does not exist")
                        controller.message = "Email did not exist. Please try again"
                        self.present(controller, animated: true, completion: nil)
                    }
                }
            }
            else {
                print("Repeated name add")
                controller.message = "Already chatting with person, please enter another email"
                self.present(controller, animated: true, completion: nil)
            }
        }
        else {
            print("Tried talking to blocked")
            controller.message = "You've blocked this user. Please unblock them first if you'd like to chat."
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func addToChatList(_ userChat: Chats) {
        chatsList.append(userChat)
        chatTableView.reloadData()
    }
}
