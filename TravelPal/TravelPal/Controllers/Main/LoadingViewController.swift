//
//  LoadingViewController.swift
//  TravelPal
//
//  Created by Matt Bommer on 11/5/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit

// This is a loading screen view controller that gives our app time to grab the necessary data from the database.
class LoadingViewController: UIViewController {

    @IBOutlet weak var loadLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //When 
    override func viewDidAppear(_ animated: Bool) {
        let queue = DispatchQueue(label: "timer", qos: .default)
        queue.async {
            while appUser == nil {
                continue
            }
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "loadSegue", sender: nil)
            }
        }
    }

}
