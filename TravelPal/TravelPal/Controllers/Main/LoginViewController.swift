//
//  LoginViewController.swift
//  TravelPal
//
//  Created by Sooyong Lee on 10/20/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

var appUser: AppUser! {
    didSet {
        appUser.tripListener()
    }
}
var db: Firestore!
var storage: Storage!

class LoginViewController: UIViewController {

    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var textFieldAge: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var segCtrl: UISegmentedControl!
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    @IBOutlet weak var textFieldLoginEmail: UITextField!
    @IBOutlet weak var textFieldLoginPassword: UITextField!
    @IBOutlet weak var textFieldLoginConfirmPassword: UITextField!
    @IBOutlet weak var loginOutlet: UIButton!
    @IBOutlet weak var signUpOutlet: UIButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        db = Firestore.firestore()
        storage = Storage.storage()
        // Log user in automatically
        Auth.auth().addStateDidChangeListener() {
          auth, user in

          if user != nil {
            self.textFieldLoginEmail.text = nil
            self.textFieldLoginPassword.text = nil
            self.textFieldLoginConfirmPassword.text = nil
            self.readUserFromDatabase((user?.email)!)
            self.performSegue(withIdentifier: "loginSegue", sender: nil)
          }
        }
    }
    
    func readUserFromDatabase(_ email: String) {
        //Read the user info from database
        print("IN READ USER DATABASE")
        let docRef = db.collection("users").document(email)
        docRef.getDocument{ (document, error) in
            if let document = document, document.exists {
                
                let userData = document.data()
                appUser = AppUser(userData!)
                // Get the user's profile image from the storage db
                let pathReference = storage.reference(withPath: "userImages/\(email).png")
                        pathReference.getData(maxSize: 20 * 1024 * 1024) { data, error in
                          if let error = error {
                            print("An error occured while trying to download the picture: \(error)")
                          } else {
                            print("This occurred So the data should be set")
                            appUser.profilePicture = UIImage(data: data!)!
                          }
                        }
            } else {
                print("Document does not exist")
            }
        }
    }
    
    @IBAction func onSegmentChanged(_ sender: Any) {
        switch segCtrl.selectedSegmentIndex {
        case 0:
            confirmPasswordLabel.isHidden = true
            textFieldLoginConfirmPassword.isHidden = true
            loginOutlet.isHidden = false
            loginOutlet.layer.cornerRadius = 5
            signUpOutlet.isHidden = true
        case 1:
            confirmPasswordLabel.isHidden = false
            textFieldLoginConfirmPassword.isHidden = false
            loginOutlet.isHidden = true
            signUpOutlet.isHidden = false
            signUpOutlet.layer.cornerRadius = 5
        default: break
        }
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        guard let email = textFieldLoginEmail.text,
            let password = textFieldLoginPassword.text,
            email.count > 0,
            password.count > 0
        else {
            return
        }
        Auth.auth().signIn(withEmail: email, password: password) {
            user, error in
            if let error = error, user == nil {
                let alert = UIAlertController(title: "Log In Failed", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK", style:.default))
                self.present(alert, animated: true, completion: nil)
            }
            //Successful login
            else {
                UserDefaults.standard.set(email, forKey: "email")
                UserDefaults.standard.set(password, forKey: "password")
                self.readUserFromDatabase(email)
                self.performSegue(withIdentifier: "loginSegue", sender: self)
            }
        }
    }
    @IBAction func signUpPressed(_ sender: Any) {
        guard let email = textFieldLoginEmail.text,
            let password = textFieldLoginPassword.text,
            let confirmPassword = textFieldLoginConfirmPassword.text,
            password == confirmPassword
            else {
                let alert = UIAlertController(title: "Sign Up Failed", message: "Please use a valid email address and/or matching passwords", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK", style:.default))
                self.present(alert, animated: true, completion: nil)
                return
        }
        Auth.auth().createUser(withEmail: email, password: password) {
            user, error in
            if error == nil && (password == confirmPassword) {
                Auth.auth().signIn(withEmail: email, password: password)
                UserDefaults.standard.set(email, forKey: "email")
                UserDefaults.standard.set(password, forKey: "password")
                //Save the singup email to database
                appUser = AppUser(email)
                db.collection("users").document(email).setData(appUser.dbFormat())  { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        print("Document successfully written!")
                    }
                }
                
                self.performSegue(withIdentifier: "loginSegue", sender: self)
            }
            else {
                let alert = UIAlertController(title: "Sign Up Failed", message: error?.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title:"OK", style:.default))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // code to enable tapping on the background to remove software keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let loadView = segue.destination as? LoadingViewController {
            loadView.modalPresentationStyle = .fullScreen
        }
    }
}
