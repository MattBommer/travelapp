//
//  TabBarViewController.swift
//  TravelPal
//
//  Created by Eric on 10/20/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.selectedIndex = 0
    }
    
    override var shouldAutomaticallyForwardAppearanceMethods: Bool {
        return true
    }
    
}
