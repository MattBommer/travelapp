//
//  PopularViewController.swift
//  TravelPal
//
//  Created by Matt Bommer on 10/21/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit

//TableViewController to showcase all the currently popular trips.
class PopularViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UISearchControllerDelegate {
    
    var search:UISearchController!
    @IBOutlet weak var popularButton: UIBarButtonItem!
    @IBOutlet weak var popularTableView: UITableView!
    var popularTrips: [Trip] = [] {
        didSet {
            self.tripCount = self.popularTrips.count
        }
    }
    var tempPopularTrips: [Trip] = []
    var searchTrips: [Trip] = []
    var isPopular: Bool = false
    let seeTripSegueIdentifier = "seeTripSegueIdentifier"
    var tripCount = 0
    var refreshController:UIRefreshControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "All Trips"
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.orange
        
        search = ({
                let controller = UISearchController(searchResultsController: nil)
                controller.searchResultsUpdater = self
                controller.searchBar.sizeToFit()
                popularTableView.tableHeaderView = controller.searchBar
                return controller
            })()
        
        popularTableView.delegate = self
        popularTableView.dataSource = self
        refreshController = UIRefreshControl()
        refreshController.attributedTitle = NSAttributedString(string: "Fetching new trips...")
        refreshController.addTarget(self, action: #selector(newTrips), for: .valueChanged)
        popularTableView.refreshControl = refreshController
    }
    
    // Add trips into the table view
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        if tripCount < appUser.popularTrips.count && !isPopular{
            popularTrips = appUser.popularTrips
            // Sort trips by the number of likes they have
            popularTrips.sort(by: {$0.startDate > $1.startDate })
            self.popularTableView.beginUpdates();
            let indexPaths = (0..<(popularTrips.count)).map { IndexPath(row: $0, section: 0)}
            popularTableView.insertRows(at: indexPaths, with: .automatic)
            self.popularTableView.endUpdates();
            
            // Unhighlight the previously selected row
            let row: IndexPath? = popularTableView.indexPathForSelectedRow
            if let selected = row {
                popularTableView.deselectRow(at: selected, animated: true)
            }
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        searchTrips.removeAll(keepingCapacity: false)
        let array = popularTrips.filter { $0.tripLocation.contains(searchController.searchBar.text!) != false || $0.tripDescription.contains(searchController.searchBar.text!)
            || $0.tripName.contains(searchController.searchBar.text!)
        }
        searchTrips = array

        self.popularTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // Instantiate the tableView for use and number of cells
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if search.isActive {
            return searchTrips.count
        }
        return popularTrips.count
    }
    
    // Fill the trips table view cell with the necessary data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "popularTripCell", for: indexPath as IndexPath) as! PopularTableViewCell
        let row = indexPath.row
        let trip = search.isActive ? searchTrips[row] : popularTrips[row]
        cell.setImage(image: trip.tripPicture)
        cell.tripName.text = trip.tripName
        cell.tripName.sizeToFit()
        cell.numberOfLikes.text = "\(trip.likeCount) Likes"
        cell.numberOfLikes.sizeToFit()
        if trip.liked {
            cell.likeButton.setImage(UIImage(systemName: "hand.thumbsup.fill"), for: .normal)
        } else {
            cell.likeButton.setImage(UIImage(systemName: "hand.thumbsup"), for: .normal)
        }
        cell.tripLocation.text = trip.tripLocation
        
        let df = DateFormatter()
        df.dateFormat = "MMM d"
        let start = df.string(from: trip.startDate)
        let end = df.string(from: trip.endDate)
        cell.tripDate.text = "\(start) - \(end)"
        cell.tripDate.sizeToFit()
        cell.trip = trip
        
        return cell
    }
    
    
    @IBAction func clickedPopular(_ sender: Any) {
        if isPopular == false {
            isPopular = true
            self.navigationItem.title = "Popular Trips"
            popularButton.image = UIImage(systemName: "gobackward")
            tempPopularTrips = popularTrips
            popularTrips.sort(by: { $0.likeCount > $1.likeCount})
            var maxPopularNum = popularTrips.count > 10 ? 10: popularTrips.count
            popularTrips = Array(popularTrips[0..<maxPopularNum])
            popularTableView.reloadData()
        }
        else {
            self.navigationItem.title = "All Trips"
            popularButton.image = UIImage(named: "flame")
            isPopular = false
            popularTrips = tempPopularTrips
            popularTableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == seeTripSegueIdentifier,
           let nextVC = segue.destination as? SeeTripViewController,
           let selectedIndex = popularTableView.indexPathForSelectedRow?.row {
            nextVC.trip = popularTrips[selectedIndex]
            }
    }
    
    @objc func newTrips() {
        if tripCount < appUser.popularTrips.count {
            let indexs = (tripCount..<appUser.popularTrips.count)
            let newTripsArr = indexs.map { appUser.popularTrips[$0] }
            popularTrips += newTripsArr
            let indexPaths = indexs.map { IndexPath(row: $0, section: 0)}
            popularTableView.beginUpdates()
            popularTableView.insertRows(at: indexPaths, with: .automatic)
            popularTableView.endUpdates()
            
            popularTableView.reloadData()
        }
        refreshController.endRefreshing()
    }
}
