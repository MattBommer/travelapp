//
//  CreateTripViewController.swift
//  TravelPal
//
//  Created by Matt Bommer on 10/22/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit
import MapKit

// View controller gives you the ability to add new trips in
class CreateTripViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var tripImage: UIImageView!
    @IBOutlet weak var endAge: UITextField!
    @IBOutlet weak var startAge: UITextField!
    @IBOutlet weak var genderPref: UISegmentedControl!
    @IBOutlet weak var tripVisibility: UISegmentedControl!
    @IBOutlet weak var startDate: UITextField!
    @IBOutlet weak var endDate: UITextField!
    @IBOutlet weak var tripName: UITextField!
    @IBOutlet weak var tripDescription: UITextView!
    var delegate: UIViewController!
    var tripMetaData: MKMapItem!
    var tripAnnotation: MKPointAnnotation?
    var defaultImage:UIImage?
    var errorMsg:String = ""
    var imagePickerObject:ImagePicker!
    var lat: Double = 0.0
    var long: Double = 0.0
    var tripLocation = ""
    var ages:[String] = []
    var travelers:[String] = [appUser.email]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Make A Trip"
        self.tripDescription.clipsToBounds = true
        self.tripDescription.layer.cornerRadius = 10.0
        self.tripDescription.layer.borderWidth = 0.5
        self.tripDescription.layer.borderColor = UIColor(displayP3Red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0).cgColor
        
        endAge.agePicker(target: self, doneAction: #selector(doneAction), cancelAction: #selector(cancelAction))
        startAge.agePicker(target: self, doneAction: #selector(doneAction), cancelAction: #selector(cancelAction))
        
        self.imagePickerObject = ImagePicker(presentationController: self, delegate: self)
        
        startDate.datePicker(target: self,
                              doneAction: #selector(doneAction),
                              cancelAction: #selector(cancelAction),
                              datePickerMode: .date)
        endDate.datePicker(target: self,
                            doneAction: #selector(doneAction),
                            cancelAction: #selector(cancelAction),
                            datePickerMode: .date)
        
        // Set the valid ages for the picker view
        for n in 18...100 {
            ages.append(String(n))
        }
        
        if tripAnnotation != nil{
            navigationItem.title = tripAnnotation?.title
            tripLocation = tripAnnotation?.subtitle ?? ""
            lat = (tripAnnotation?.coordinate.latitude)!
            long = (tripAnnotation?.coordinate.longitude)!
        }else {
            navigationItem.title = tripMetaData.name
            tripLocation = tripMetaData.placemark.title!
            long = (tripMetaData.placemark.location?.coordinate.longitude)!
            lat = (tripMetaData.placemark.location?.coordinate.latitude)!
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        defaultImage = tripImage.image
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ages.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ages[row]
    }
    
    @IBAction func addPhotoPressed(_ sender: UIButton) {
        imagePickerObject.present(from: sender)
    }
    
    // Create a new trip once all data is set and done was pressed
    @IBAction func doneButtonPressed(_ sender: Any) {
        //Send user to the trip details page.
        if blankField() {
            let alert = UIAlertController(title: "Incorrect Trip Details", message: self.errorMsg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
        }else {
            let userTrip = Trip(userTrip: self)
            userTrip.storeTripInDatabase()
            appUser.addTrip(trip: userTrip)
            addPin(newTrip: userTrip)
            navigationController?.popViewController(animated: true)
        }
    }
    
    func addPin(newTrip: Trip) {
        let mapVC = delegate as! MapViewController
        if tripAnnotation == nil {
            let tripAnn = MKPointAnnotation()
            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            tripAnn.coordinate = coordinate
            tripAnn.title = tripMetaData.placemark.name
            tripAnn.subtitle = tripMetaData.placemark.title
            tripAnn.accessibilityElements = [newTrip]
            mapVC.travelMap.addAnnotation(tripAnn)
            mapVC.goToLocation(center: coordinate)
        }else {
            tripAnnotation!.accessibilityElements = [newTrip]
            mapVC.travelMap.addAnnotation(tripAnnotation!)
            mapVC.goToLocation(center: tripAnnotation?.coordinate)
        }
    }
        
    // Method checks to see that the user set all the necessary data fields
    func blankField () -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if endAge.text == "" {
            errorMsg = "The maximum age was left blank"
            return true
        }else if tripName.text == "" {
            errorMsg = "The trip name was left blank"
            return true
        }else if tripDescription.text == "" {
            errorMsg = "The trip description was left blank"
            return true
        }else if startAge.text == "" {
            errorMsg = "The minimum age was left blank"
            return true
        }else if startDate.text == "" || endDate.text == "" {
            errorMsg = "The start date and/or the end date were never set"
            return true
        }else if dateFormatter.date(from: startDate.text!)! > dateFormatter.date(from: endDate.text!)! {
            errorMsg = "The start date can't be after the end date"
            return true
        }else if Int(startAge.text!) ?? 18 > Int(endAge.text!) ?? 100 {
            errorMsg = "The min age can't be greater than the max age"
            return true
        }else if tripImage.image?.cgImage == defaultImage?.cgImage && tripImage.image?.ciImage == defaultImage?.ciImage {
            errorMsg = "Trip image was never set"
            return true
        }else if Int(startAge.text!) ?? 100 > Int(appUser.age) ?? 18 || Int(endAge.text!) ?? 100 < Int(appUser.age) ?? 18 {
            errorMsg = "Your age must be within your age range"
            return true
        }
        return false
    }

    @objc
    func cancelAction() {
        if startDate.isFirstResponder {
            startDate.resignFirstResponder()
        }else if endDate.isFirstResponder {
            endDate.resignFirstResponder()
        }else if startAge.isFirstResponder {
            startAge.resignFirstResponder()
        }else if endAge.isFirstResponder {
            endAge.resignFirstResponder()
        }
    }

    @objc
    func doneAction() {
        if startDate.isFirstResponder || endDate.isFirstResponder {
            var textField = startDate
            if endDate.isFirstResponder {
                textField = endDate
            }
            if let datePickerView = textField!.inputView as? UIDatePicker {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let dateString = dateFormatter.string(from: datePickerView.date)
                textField!.text = dateString
                
                textField!.resignFirstResponder()
            }
        }else {
            var textField = startAge
            if endAge.isFirstResponder {
                textField = endAge
            }
            if let agePickerView = textField!.inputView as? UIPickerView {
                let row = agePickerView.selectedRow(inComponent: 0)
                textField!.text = ages[row]
                
                textField!.resignFirstResponder()
            }
            
        }
    }

    // Button spawns a UIAlertController for adding travelers by their emails
//    @IBAction func addTravelerPressed(_ sender: Any) {
//        let alert = UIAlertController(title: "Add a Traveler", message: nil, preferredStyle: .alert)
//        alert.addTextField()
//        alert.textFields?[0].placeholder = "Traveler email goes here"
//        let addTrav = UIAlertAction(title: "Enter", style: .default) {
//            [unowned alert] _ in
//            let trav = alert.textFields![0]
//            if trav.text?.count ?? 0 > 0 {
//                self.travelers.append(trav.text!)
//            }
//        }
//        let cancelTrav = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//        alert.addAction(addTrav)
//        alert.addAction(cancelTrav)
//        present(alert, animated: true)
//    }
    
}
// Sets the UIImage for a trip (picks picture from camera roll)
extension CreateTripViewController: ImagePickerDelegate {
    
    func didSelect(image: UIImage?) {
        self.tripImage.image = image
    }
}

//Code was adapted from https://programmingwithswift.com/add-uidatepicker-in-a-uitextfield-with-swift/
// This allows us to place date pickers inside the desired textfields
extension UITextField {
    func datePicker<T>(target: T,
                       doneAction: Selector,
                       cancelAction: Selector,
                       datePickerMode: UIDatePicker.Mode = .date) {
            let screenWidth = UIScreen.main.bounds.width
            func buttonItem(withSystemItemStyle style: UIBarButtonItem.SystemItem) -> UIBarButtonItem {
                let buttonTarget = style == .flexibleSpace ? nil : target
                let action: Selector? = {
                    switch style {
                    case .cancel:
                        return cancelAction
                    case .done:
                        return doneAction
                    default:
                        return nil
                    }
                }()
                
                let barButtonItem = UIBarButtonItem(barButtonSystemItem: style, target: buttonTarget, action: action)
                
                return barButtonItem
            }
            let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
            datePicker.datePickerMode = datePickerMode
            datePicker.preferredDatePickerStyle = .wheels
            self.inputView = datePicker
            let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
            toolBar.setItems([buttonItem(withSystemItemStyle: .cancel), buttonItem(withSystemItemStyle: .flexibleSpace), buttonItem(withSystemItemStyle: .done)], animated: true)
            self.inputAccessoryView = toolBar
        }
    
    func agePicker<T>(target: T,
                       doneAction: Selector,
                       cancelAction: Selector) {
            let screenWidth = UIScreen.main.bounds.width
            func buttonItem(withSystemItemStyle style: UIBarButtonItem.SystemItem) -> UIBarButtonItem {
                let buttonTarget = style == .flexibleSpace ? nil : target
                let action: Selector? = {
                    switch style {
                    case .cancel:
                        return cancelAction
                    case .done:
                        return doneAction
                    default:
                        return nil
                    }
                }()
                
                let barButtonItem = UIBarButtonItem(barButtonSystemItem: style, target: buttonTarget, action: action)
                
                return barButtonItem
            }
            let agePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
            self.inputView = agePicker
            let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
            toolBar.setItems([buttonItem(withSystemItemStyle: .cancel), buttonItem(withSystemItemStyle: .flexibleSpace), buttonItem(withSystemItemStyle: .done)], animated: true)
            self.inputAccessoryView = toolBar
            agePicker.dataSource = target as? UIPickerViewDataSource
            agePicker.delegate = target as? UIPickerViewDelegate
        }
}
