//
//  MapViewController.swift
//  TravelPal
//
//  Created by Matt Bommer on 10/21/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate, UISearchResultsUpdating, UISearchControllerDelegate {

    @IBOutlet weak var travelMap: MKMapView!
    var search:UISearchController!
    var searchResults:UITableViewController!
    var results:[MKMapItem] = []

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        travelMap.showsUserLocation = true
        travelMap.delegate = self
        let pinGest = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gestRec:)))
        travelMap.addGestureRecognizer(pinGest)
        
        searchResults = UITableViewController()
        searchResults.tableView.delegate = self
        searchResults.tableView.dataSource = self
        // Set this to the custom tableviewcell to make it appear
        searchResults.tableView.register(SearchTableViewCell.self, forCellReuseIdentifier: SearchTableViewCell.identifier)
        
        search = UISearchController(searchResultsController: searchResults)
        search.delegate = self
        search.searchBar.placeholder = "Search Locations"
        navigationItem.titleView = search.searchBar
        search.hidesNavigationBarDuringPresentation = false
        search.searchResultsUpdater = self
        definesPresentationContext = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "list.bullet"), style: .plain, target: self, action: #selector(popularView))
        
        let userLoc = travelMap.userLocation
        let center = userLoc.location?.coordinate
        goToLocation(center: center)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tripPins()
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let annotation = view.annotation as! MKPointAnnotation
        let trip = annotation.accessibilityElements?[0] as! Trip
        performSegue(withIdentifier: "seeTripSegueIdentifier", sender: trip)
    }
    
    func goToLocation(center: CLLocationCoordinate2D?) {
        if center != nil {
            let horizontal = 2500000.0
            let vertical = 2500000.0
            
            let region = MKCoordinateRegion (
                center: center!,
                latitudinalMeters: horizontal,
                longitudinalMeters: vertical)
            
            travelMap.setRegion(region, animated: true)
        }
    }
    
    func tripPins() {
        travelMap.removeAnnotations(travelMap.annotations)
        let allTrips  = appUser.trips + appUser.popularTrips
        var annotations:[MKPointAnnotation] = []
        for trip in allTrips {
            let coordinate = CLLocationCoordinate2D(latitude: trip.lat, longitude: trip.long)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            annotation.title = trip.tripName
            annotation.subtitle = trip.tripLocation
            annotation.accessibilityElements = [trip]
            annotations.append(annotation)
        }
        travelMap.addAnnotations(annotations)
    }

    func updateSearchResults(for searchController: UISearchController) {
        results.removeAll(keepingCapacity: false)
        let searchRequests = searchController.searchBar.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchRequests
        request.region = MKCoordinateRegion(.world)
        request.pointOfInterestFilter = MKPointOfInterestFilter.init(including: [.museum, .nationalPark, .amusementPark, .beach, .nightlife, .stadium])
        let search = MKLocalSearch(request: request)
        
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }
            self.results = response.mapItems
            
            self.searchResults.tableView.reloadData()
        }
    }
    
    @objc func popularView() {
        performSegue(withIdentifier: "popularSegue", sender: nil)
    }
    
    @objc func longPress(gestRec: UIGestureRecognizer) {
        guard gestRec.state == .began else { return }
        let touchPoint = gestRec.location(in: travelMap)
        let coordinates = travelMap.convert(touchPoint, toCoordinateFrom: travelMap)
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinates
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude), completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                print("An error occurred when trying to reverse search the given coordinates | \(String(describing: error))")
                return
            }
            if placemarks?.count ?? 0 > 0 {
                let place = placemarks?[0]
                annotation.title = place?.name
                let subtitle = [place?.subLocality, place?.locality, place?.administrativeArea, place?.country]
                var finalSubtitle:[String] = []
                for element in subtitle {
                    if element != nil {
                        finalSubtitle.append(element!)
                    }
                }
                annotation.subtitle = finalSubtitle.joined(separator: ", ")
            }else {
                annotation.title = "Unknown"
                annotation.subtitle = "Unknown"
            }
        })
        performSegue(withIdentifier: "pinSegue", sender: annotation)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createTripSegue"
        {
            let createTripVC = segue.destination as! CreateTripViewController
            createTripVC.tripMetaData = self.results[searchResults.tableView.indexPathForSelectedRow!.row]
            createTripVC.delegate = self
        }else if segue.identifier == "pinSegue" {
            let createTripVC = segue.destination as! CreateTripViewController
            createTripVC.tripMetaData = MKMapItem()
            createTripVC.tripAnnotation = sender as? MKPointAnnotation
            createTripVC.delegate = self
        }
        if segue.identifier == "seeTripSegueIdentifier",
           let nextVC = segue.destination as? SeeTripViewController
           {
                nextVC.trip = sender as? Trip
           }
        }

    
    
}
extension MapViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.identifier, for: indexPath) as! SearchTableViewCell
        if results.count != 0 {
            let data = results[indexPath.row]
            cell.name.text = data.placemark.name
            cell.address.text = data.placemark.title
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "createTripSegue", sender: nil)
    }
    
  }
