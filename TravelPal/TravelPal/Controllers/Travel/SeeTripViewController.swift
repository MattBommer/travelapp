//
//  SeeTripViewController.swift
//  TravelPal
//
//  Created by Kim Ha on 11/4/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit
import Foundation


class TravelersTableViewCell: UITableViewCell {
    @IBOutlet var pictureView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var ageLabel: UILabel!
    // MATT: Make AgePref and GenderPref label connections
}

class SeeTripViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var tripImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet var genderLabel: UILabel!
    @IBOutlet var ageLabel: UILabel!
    @IBOutlet var joinButton: UIButton!
    @IBOutlet var leaveButton: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet var travelersTableView: UITableView!
    
    let tableCellIdentifier = "travelerCellIdentifier"
    

    // When you segue to a trip, set the trip
    var trip:Trip!
    var travelersDict = [String:AppUser]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Update any information about a trip that may have changed
        updateTrip()
        // Show the join button if the user is not already in the trip
        if(trip.travelers.contains(appUser.email)) {
            joinButton.isHidden = true
            leaveButton.isHidden = false
        } else {
            joinButton.isHidden = false
            leaveButton.isHidden = true
        }

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd, yyyy"
        
        // Change labels to reflect the trip information
        tripImage.image = trip.tripPicture 
        dateLabel.text = "\(dateFormatterPrint.string(from: trip.startDate)) - \(dateFormatterPrint.string(from: trip.endDate))"
        locationLabel.text = "\(trip.tripLocation)"
        // Set the title of the page to the trip name
        navigationItem.title = trip.tripName
        // Set the description
        descriptionTextView.text = trip.tripDescription
        descriptionTextView.isEditable = false
        // Set the age ranges
        ageLabel.text = "\(trip.startAge) - \(trip.endAge)"
        // Set the gender preference
        genderLabel.text = "\(trip.genderPref)"
        
        
        travelersTableView.delegate = self
        travelersTableView.dataSource = self

        // Get the appUser information for each email in trip.travelers
        for traveler in trip.travelers {
            readUserFromDatabase(traveler)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        // Update any information about a trip that may have changed
        updateTrip()

        // Refresh the appUser information in case new users have joined the trip
        for traveler in trip.travelers {
            if !travelersDict.keys.contains(traveler) {
                readUserFromDatabase(traveler)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        travelersTableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    // Return the number of cells to display
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travelersDict.count
    }

    // make a cell for each cell index path
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get a table view cell
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier, for: indexPath as IndexPath) as! TravelersTableViewCell
        let (otherEmail, otherUser) = Array(travelersDict)[indexPath.row]

        // Set its UI elements
        cell.nameLabel.text = otherUser.name
        cell.pictureView.image = otherUser.profilePicture
        cell.emailLabel.text = otherEmail
        cell.ageLabel.text = "\(otherUser.age)"

        return cell
    }


    // read a user from the database using their email
    // create an appUser object for them
    // add the appUser to the list of travelers
    func readUserFromDatabase(_ email: String){
        let docRef = db.collection("users").document(email)
        docRef.getDocument{ (document, error) in
            if let document = document, document.exists {

                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                print("Document data: \(dataDescription)")

                let userData = document.data()
                let appUser = AppUser(userData!)
                self.travelersDict.updateValue(appUser, forKey: appUser.email)
                
                // Get the user's profile image from the storage db
                let pathReference = storage.reference(withPath: "userImages/\(appUser.email).png")
                        pathReference.getData(maxSize: 20 * 1024 * 1024) { data, error in
                          if let error = error {
                            print("An error occured while trying to download the picture: \(error)")
        
                          } else {
                            print("Image was successfully retrieved from database")
                            appUser.profilePicture = UIImage(data: data!)!
                            
                          }
                            // reload the table view with new data
                            self.travelersTableView.reloadData()
                        }
                
            } else {
                print("Document does not exist")
            }
        }
    }

    @IBAction func joinButtonPressed(_ sender: Any) {
        // Add the user as a traveler if they are of the right age
        let userAge = Int(appUser.age) ?? 0
        let startAge = Int(trip.startAge) 
        let endAge = Int(trip.endAge)
        if userAge >= startAge && userAge <= endAge {
            // only add them if they are not already in the trip
            if !trip.travelers.contains(appUser.email) {
                // Add them to the list of travelers in the trip
                trip.travelers.append(appUser.email)
                travelersDict.updateValue(appUser, forKey: appUser.email)
                
                // Update the tableView to show the new user
                travelersTableView.reloadData()
                
                // Add the trip to the user's saved trips if it is not already saved
                if !appUser.tripIds.contains(trip.uniqueId) {
                    appUser.addTrip(trip: trip)
                }
                
                // Add the user to the trip
                db.collection("trips").document(trip.uniqueId).updateData([
                    "travelers": trip.travelers
                ]) { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        print("Document successfuly written!")
                    }
                }
            }
            joinButton.isHidden = true
            leaveButton.isHidden = false
        } else {
            let alert = UIAlertController(title: "Could not join trip", message: "You are not within the age range", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK", style:.default))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func leaveButtonPressed(_ sender: Any) {
        // Remove the user from the trip
        if let index = trip.travelers.firstIndex(of: appUser.email) {
            trip.travelers.remove(at: index)
        }
        // remove the user from our list searching by emails
        travelersDict.removeValue(forKey: appUser.email)
        
        // Update the tableView to show that the user has left the trip
        travelersTableView.reloadData()

        // Remove the user from the trip in the db
        db.collection("trips").document(trip.uniqueId).updateData([
            "travelers": trip.travelers
        ]) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfuly written!")
            }
        }
        
        // Change the button to join trip
        joinButton.isHidden = false
        leaveButton.isHidden = true
    }
    
    // Check for updates to a trip based on its tripID
    func updateTrip () {
        let docRef = db.collection("trips").document(trip.uniqueId)

        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                self.trip.travelers = document.data()?["travelers"] as? [String] ?? []
                print("new travelers list: ", self.trip.travelers)
            } else {
                print("Document does not exist")
            }
        }
    }
    
    /*
     MARK: - Navigation

     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         Get the new view controller using segue.destination.
         Pass the selected object to the new view controller.
    }
    */

}
