//
//  SavedViewController.swift
//  TravelPal
//
//  Created by Matt Bommer on 10/22/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit

// View controller for a user's saved trips
class SavedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var savedTableView: UITableView!
    var savedTrips:[Trip] = []
    
    let seeTripSegueIdentifier = "seeTripSegueIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Saved Trips"
        savedTableView.dataSource = self
        savedTableView.delegate = self
    }
    
    // When the view appears on screen we want to add all of the saved trips to our table view.
    override func viewDidAppear(_ animated: Bool) {
        let currCount = savedTrips.count
        if currCount < appUser.trips.count {
            for trip in appUser.trips[currCount...] {
                savedTrips.append(trip)
            }
            self.savedTableView.beginUpdates();
            for ind in currCount...(savedTrips.count - 1){
                savedTableView.insertRows(at: [IndexPath(row: ind, section: 0)], with: .automatic)
            }
            self.savedTableView.endUpdates();
        }
        
        // Deselect rows so they don't remain highlighted all the time
        let row: IndexPath? = savedTableView.indexPathForSelectedRow
        if let selected = row {
            savedTableView.deselectRow(at: selected, animated: true)
        }
    }
    
    // Sets the number of sections for the table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // Instantiate the tableView for use and number of cells
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedTrips.count
    }
    
    // Fill our custom table view cell with the necessary data.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "popularTripCell", for: indexPath as IndexPath) as! PopularTableViewCell
        let row = indexPath.row
        let trip = savedTrips[row]
        cell.setImage(image: trip.tripPicture)
        cell.tripName.text = trip.tripName
        cell.numberOfLikes.text = "\(trip.likeCount) Likes"
        cell.numberOfLikes.sizeToFit()
        cell.tripLocation.text = trip.tripLocation
        cell.tripLocation.sizeToFit()
        if trip.liked {
            cell.likeButton.setImage(UIImage(systemName: "hand.thumbsup.fill"), for: .normal)
        } else {
            cell.likeButton.setImage(UIImage(systemName: "hand.thumbsup"), for: .normal)
        }
        
        let df = DateFormatter()
        df.dateFormat = "MMM d"
        let start = df.string(from: trip.startDate)
        let end = df.string(from: trip.endDate)
        cell.tripDate.text = "\(start) - \(end)"
        cell.tripDate.sizeToFit()
        cell.trip = trip
        
        return cell
    }
    
    // Method that allows users to delete a trip from their saved trips.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
                savedTrips.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                appUser.removeTrip(index: indexPath.row)
        }
    }
    
    // Send the trip to SeeTripVC so that trip data can be expanded on.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == seeTripSegueIdentifier,
           let nextVC = segue.destination as? SeeTripViewController,
           let selectedIndex = savedTableView.indexPathForSelectedRow?.row {
                nextVC.trip = savedTrips[selectedIndex]
            }
    }
    
}
