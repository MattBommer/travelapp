//
//  CalendarViewController.swift
//  TravelPal
//
//  Created by Matt Bommer on 10/21/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit
import FSCalendar

// This will be implememented in the final when we give functionality to the calendar
class CalendarViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var calendarObj: FSCalendar!
    @IBOutlet weak var calendarTableView: UITableView!
    
    var tripsOnDate:[Trip] = []
    var popTripsOnDate:[Trip] = []
    let today = Date()
    
    let tripCellIdentifier = "popularTripCell"
    let seeTripSegueIdentifier = "seeTripSegueIdentifier"
    
    fileprivate lazy var dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendarObj.dataSource = self;
        calendarObj.delegate = self;
        calendarTableView.dataSource = self;
        calendarTableView.delegate = self;
        calendarObj.appearance.todayColor = UIColor(red: 136/255, green: 160/255, blue: 168/255, alpha: 1.0) /* #88A0A8 */
        
        // Load in the saved trips that are on today's date
        tripsOnDate = searchTripsByDate(tripList: appUser.trips, date: today)
        tripsOnDate += searchTripsByDate(tripList: appUser.popularTrips, date: today)
    }
    
    // If the page is reloaded, reload the trips
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        tripsOnDate = searchTripsByDate(tripList: appUser.trips, date: today)
        tripsOnDate += searchTripsByDate(tripList: appUser.popularTrips, date: today)
        calendarTableView.reloadData()
    }
    
    // Returns an array of trips that have a start date matching the given date
    private func searchTripsByDate(tripList: [Trip], date: Date) -> [Trip] {
        var result:[Trip] = []
        let dateString = dateFormatter.string(from: date)
        
        for trip in tripList {
            let tripDateString = dateFormatter.string(from: trip.startDate)
            if tripDateString == dateString {
                result.append(trip)
            }
        }
        
        return result
    }
    
    // Show a flame icon if the date has an event on popular trips and a heart if it's on saved
    func calendar(_ calendar: FSCalendar!, imageFor date: Date) -> UIImage! {
        let dateString = dateFormatter.string(from: date)
        
        for trip in appUser.trips {
            let startString = dateFormatter.string(from: trip.startDate)
            if startString == dateString {
                return UIImage(named: "heart")?.resized(toWidth: 15)?.withTintColor(UIColor.red)
            }
        }
        
        for trip in appUser.popularTrips {
            let startString = dateFormatter.string(from: trip.startDate)
            if startString == dateString {
                return UIImage(named: "flame")?.resized(toWidth: 15)?.withTintColor(UIColor.orange)
            }
        }
        
        return nil
    }
    

    
    // Update the tableview with events when a new date is selected
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        // search saved trips for every trip on the selected date and update tripsOnDate
        tripsOnDate = searchTripsByDate(tripList: appUser.trips, date: date)
        tripsOnDate += searchTripsByDate(tripList: appUser.popularTrips, date: date)
        calendarTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tripsOnDate.count
    }
    
    // Fill the trips table view cell with the necessary data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tripCellIdentifier, for: indexPath as IndexPath) as! PopularTableViewCell
        let trip = tripsOnDate[indexPath.row]
        
        cell.setImage(image: trip.tripPicture)
        cell.tripLocation.text = trip.tripLocation
        cell.tripName.text = trip.tripName
        
        let df = DateFormatter()
        df.dateFormat = "MMM d"
        let start = df.string(from: trip.startDate)
        let end = df.string(from: trip.endDate)
        cell.tripDate.text = "\(start) - \(end)"
        cell.trip = trip
        
        return cell
    }
    
    // Send the trip to SeeTripVC so we can display more of its data
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == seeTripSegueIdentifier,
           let nextVC = segue.destination as? SeeTripViewController,
           let selectedIndex = calendarTableView.indexPathForSelectedRow?.row {
                nextVC.trip = tripsOnDate[selectedIndex]
            }
    }
    
    
}

extension UIImage {
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
