//
//  ProfileViewController.swift
//  TravelPal
//
//  Created by Kim Ha on 10/21/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit

protocol ProfileUpdater {
    func updateProfile(image:UIImage, name:String, age:String, location:String, bio:String, dest0:String, dest1:String, dest2:String)
}

class ProfileViewController: UIViewController, ProfileUpdater {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var dreamDestinationLabel0: UILabel!
    @IBOutlet weak var dreamDestinationLabel1: UILabel!
    @IBOutlet weak var dreamDestinationLabel2: UILabel!

    let profileSegue = "profileSegue"
    let accountSegue = "accountSegue"
    let blockedSegue = "blockedSegue"

    override func viewDidLoad() {
        super.viewDidLoad()
        loadProfile()
    }
    
    // Display the user's profile information in the correct labels
    func loadProfile(){
        profileImageView.image = appUser.profilePicture
        nameLabel.text = appUser.name
        ageLabel.text = appUser.age
        locationLabel.text = appUser.location
        bioLabel.text = appUser.bio
        dreamDestinationLabel0.text = appUser.dest0
        dreamDestinationLabel1.text = appUser.dest1
        dreamDestinationLabel2.text = appUser.dest2
    }
    
    // Display a menu with Account Settings, Profile Settings, and Blocked List
    // Segue to the corresponding page
    @IBAction func menuButton(_ sender: Any) {
        let controller = UIAlertController(title: "Settings",
                                           message: "",
                                           preferredStyle: .actionSheet)
        
        controller.addAction(UIAlertAction(
            title: "Profile Settings",
            style: .default,
            handler: { _ in
            self.performSegue(withIdentifier: self.profileSegue, sender: nil)
            }))
        
        controller.addAction(UIAlertAction(title: "Account Settings",
            style: .default,
            handler: { _ in
            self.performSegue(withIdentifier: self.accountSegue, sender: nil)
            }))
        
        controller.addAction(UIAlertAction(title: "Blocked List",
            style: .default,
            handler: { _ in
            self.performSegue(withIdentifier: self.blockedSegue, sender: nil)
            }))
        
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
        present(controller, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If we are seguing to the profile settings, set self as delegate
        if segue.identifier  == profileSegue,
           let nextVC = segue.destination as? ProfileSettingsViewController {
            nextVC.delegate = self
        }
    }
    
    // Update the profile page with new information
    func updateProfile(image:UIImage, name:String, age:String, location:String, bio:String, dest0:String, dest1:String, dest2:String) {
        profileImageView.image = image
        nameLabel.text = name
        ageLabel.text = "\(age)"
        locationLabel.text = location
        bioLabel.text = bio
        dreamDestinationLabel0.text = dest0
        dreamDestinationLabel1.text = dest1
        dreamDestinationLabel2.text = dest2
    }

}
