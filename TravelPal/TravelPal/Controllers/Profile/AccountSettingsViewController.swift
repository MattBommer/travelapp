//
//  AccountSettingsViewController.swift
//  TravelPal
//
//  Created by Kim Ha on 10/21/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit
import FirebaseAuth

class AccountSettingsViewController: UIViewController {

    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var ageMinField: UITextField!
    @IBOutlet weak var ageMaxField: UITextField!
    @IBOutlet weak var emailAddressField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var statusLabel: UILabel!
    
    var phoneNumber = appUser.phoneNumber
    var ageMin = appUser.ageMin
    var ageMax = appUser.ageMax
    var emailAddress = UserDefaults.standard.string(forKey: "email")
    var password = UserDefaults.standard.string(forKey: "password")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAccountSetting()
    }
    override func viewDidAppear(_ animated: Bool) {
        loadAccountSetting()
    }
    
    // display the user's account settings in the fields
    func loadAccountSetting() {
        phoneField.text = appUser.phoneNumber
        ageMinField.text = appUser.ageMin
        ageMaxField.text = appUser.ageMax
        emailAddressField.text = emailAddress
        passwordField.text = password
    }
    
    // Save the user's account settings
    @IBAction func saveButton(_ sender: Any) {
        let ageMin = ageMinField.text ?? ""
        let ageMax = ageMaxField.text ?? ""
        
        // Check for invalid inputs
        if ageMin == "" || (Int(ageMin) ?? 0) < 18 {
            let alert = UIAlertController(title: "Please enter a valid age", message: "Minimum age must be at least 18", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK", style:.default))
            self.present(alert, animated: true, completion: nil)
        } else if ageMax == "" || (Int(ageMax) ?? 0) < 18 {
            let alert = UIAlertController(title: "Please enter a valid age", message: "Maximum age must be at least 18", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK", style:.default))
            self.present(alert, animated: true, completion: nil)
        } else {
            
            // Require the user to authenticate before updating account information
            let controller = UIAlertController(title:"Reauthenticate", message: "You must reauthenticate to change your account settings", preferredStyle: .alert)
            controller.addTextField(configurationHandler: {
                (textField:UITextField!) in textField.placeholder = "Enter email address"
            })
            controller.addTextField(configurationHandler: {
                (textField:UITextField!) in textField.placeholder = "Enter password"; textField.isSecureTextEntry = true
            })
            controller.addAction(UIAlertAction(title: "OK", style: .default, handler: {
                (paramAction:UIAlertAction!) in
                if let textFieldArray = controller.textFields {
                    let textFields = textFieldArray as [UITextField]
                    let email = textFields[0].text
                    let password = textFields[1].text
                    let user = Auth.auth().currentUser
                    let credential:AuthCredential = EmailAuthProvider.credential(withEmail: email!, password: password!)
                    user?.reauthenticate(with: credential, completion: {(authResult, error) in
                        if let error = error {
                            let alert = UIAlertController(title: "Failed to reauthenticate", message: error.localizedDescription, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"OK", style:.default))
                            self.present(alert, animated: true, completion: nil)
                        }
                    })
                    user!.updateEmail(to: self.emailAddressField.text!) {
                        (error) in
                        if error != nil {
                            let alert = UIAlertController(title: "Failed to change email address", message: error?.localizedDescription, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"OK", style:.default))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else {
                            UserDefaults.standard.set(self.emailAddressField.text!, forKey: "email")
                        }
                    }
                    user!.updatePassword(to: self.passwordField.text!, completion: {
                        (error) in
                        if error != nil {
                            let alert = UIAlertController(title: "Failed to change password", message: error?.localizedDescription, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title:"OK", style:.default))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else {
                            UserDefaults.standard.set(self.passwordField.text!, forKey: "password")
                        }
                    })
                    db.collection("users").document(self.emailAddress!).updateData([
                        "phoneNumber": self.phoneField.text!,
                        "ageMin": self.ageMinField.text!,
                        "ageMax": self.ageMaxField.text!
                    ]) { err in
                        if let err = err {
                            print("Error writing document: \(err)")
                        } else {
                            print("Document successfuly written!")
                        }
                    }
                    appUser.phoneNumber = self.phoneField.text!
                    appUser.ageMin = self.ageMinField.text!
                    appUser.ageMax = self.ageMaxField.text!
                    self.navigationController?.popViewController(animated: true)
                }
            }))
            present(controller, animated: true, completion: nil)
        }
        
    }
    
    // Sign the user out of the app
    @IBAction func signOutButton(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "password")
        do {
            try Auth.auth().signOut()
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextVC = storyBoard.instantiateViewController(identifier: "LoginPage")
            UIApplication.shared.windows.first?.rootViewController = nextVC
        }
        catch {
            print("Already logged out")
        }
    }
    
    // code to enable tapping on the background to remove software keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}


