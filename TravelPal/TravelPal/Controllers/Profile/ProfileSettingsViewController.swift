//
//  ProfileSettingsViewController.swift
//  TravelPal
//
//  Created by Kim Ha on 10/21/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit

class ProfileSettingsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // All of these fields will have content fetched from database
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var ageField: UITextField!
    @IBOutlet weak var locationField: UITextField!
    @IBOutlet weak var dreamDestinationField0: UITextField!
    @IBOutlet weak var dreamDestinationField1: UITextField!
    @IBOutlet weak var dreamDestinationField2: UITextField!
    @IBOutlet weak var bioField: UITextField!
    
    var imagePicker: ImagePicker!
    var delegate: ProfileViewController!
    
    var image:UIImage!
    var name = ""
    var age = ""
    var location = ""
    var dest0 = ""
    var dest1 = ""
    var dest2 = ""
    var bio = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // All initial data will be from database
        loadProfile()
        // Create an ImagePicker object
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadProfile()
    }
    
    // Display the profile settings in the fields
    func loadProfile(){
        profilePicture.image = appUser.profilePicture
        nameField.text = appUser.name
        ageField.text = appUser.age
        locationField.text = appUser.location
        bioField.text = appUser.bio
        dreamDestinationField0.text = appUser.dest0
        dreamDestinationField1.text = appUser.dest1
        dreamDestinationField2.text = appUser.dest2
    }
    
    // When the add photo button is clicked, present an image picker for the user to select an image
    @IBAction func addPhotoButton(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }

    // Save the user's new information into the database and their AppUser object
    @IBAction func saveButton(_ sender: Any) {
        
        image = profilePicture.image ?? nil
        name = nameField.text ?? ""
        age = ageField.text ?? ""
        location = locationField.text ?? ""
        dest0 = dreamDestinationField0.text ?? ""
        dest1 = dreamDestinationField1.text ?? ""
        dest2 = dreamDestinationField2.text ?? ""
        bio = bioField.text ?? ""
        
        // Check for invalid inputs
        if name == "" {
            let alert = UIAlertController(title: "Please enter a valid name", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK", style:.default))
            self.present(alert, animated: true, completion: nil)
        } else if age == "" || (Int(age) ?? 0) < 18 {
            let alert = UIAlertController(title: "Please enter a valid age", message: "You must be 18 or older to use this app.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK", style:.default))
            self.present(alert, animated: true, completion: nil)
        } else if (Int(age) ?? 0) > 100 {
            let alert = UIAlertController(title: "Please enter a valid age", message: "You are probably too old to be travelling!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK", style:.default))
            self.present(alert, animated: true, completion: nil)
        } else {
            // Change the info displayed in profile
            let profileVC = delegate as! ProfileUpdater
            
            // Update the database
            profileVC.updateProfile(image: image, name: name, age: age, location: location, bio: bio, dest0: dest0, dest1: dest1, dest2: dest2)
            db.collection("users").document(appUser.email).updateData([
                "name": name,
                "age": age,
                "location": location,
                "dest0": dest0,
                "dest1": dest1,
                "dest2": dest2,
                "bio": bio
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfuly written!")
                }
            }
            
            // Save the user's profile image with their email as the id
            let imageData = image.pngData()!
            let storageRef = storage.reference()
            let tripRef = storageRef.child("userImages/\(appUser.email).png")
            let upload = tripRef.putData(imageData, metadata: nil) {
                (metadata, error) in guard let metadata = metadata else {
                    print("An error occurred while uploading a picture \(error)")
                    return
                }
            }
        
            // Update appUser
            appUser.profilePicture = image
            appUser.name = name
            appUser.age = age
            appUser.location = location
            appUser.dest0 = dest0
            appUser.dest1 = dest1
            appUser.dest2 = dest2
            appUser.bio = bio
            navigationController?.popViewController(animated: true)
        }
    }
    
    // code to enable tapping on the background to remove software keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}

extension ProfileSettingsViewController: ImagePickerDelegate {

    func didSelect(image: UIImage?) {
        self.profilePicture.image = image
    }
}
