//
//  BlockedListViewController.swift
//  TravelPal
//
//  Created by Kim Ha on 10/21/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit


class BlockedListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var blockedTableView: UITableView!
    var blockedList:[String] = appUser.blockedList // This will be an NSManagedObject
    let cellIdentifier = "blockedCell"
    var imageList:[UIImage] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.blockedTableView.delegate = self
        self.blockedTableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated:Bool) {
        imageList.removeAll()
        for email in blockedList {
            let pathReference = storage.reference(withPath: "userImages/\(email).png")
            pathReference.getData(maxSize: 20 * 512 * 512) { data, error in
              if let error = error {
                print("An error occurred while trying to download the picture: \(error)")
                self.imageList.append(UIImage(named: "defaultProfile")!)
                self.blockedTableView.reloadData()
              } else {
                print("This occurred So the data should be set")
                self.imageList.append(UIImage(data: data!)!)
                self.blockedTableView.reloadData()
              }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // Return the number of cells to display
        return imageList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Choose a reusable cell to dispay
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath as IndexPath) as! BlockedUser
        
        // Determine which element of dataSource to display on the cell
        let row = indexPath.row
        cell.blockedNameLabel.text = blockedList[row]
        cell.blockedImageView.image = imageList[row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        // After the cell is selected, deselect
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView,
                                commit editingStyle: UITableViewCell.EditingStyle,
                                forRowAt indexPath: IndexPath) {
        
        // Delete the cell and contents from blockedList
        if editingStyle == .delete {
            blockedList.remove(at: indexPath.row)
            imageList.remove(at: indexPath.row)
            appUser.blockedList.remove(at: indexPath.row)
            db.collection("users").document(appUser.email).updateData([
                "blockedList": appUser.blockedList
            ]) { err in
                if let err = err {
                    print("Error writing document:\(err)")
                } else {
                    print("Document successfuly written!")
                }
            }
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
