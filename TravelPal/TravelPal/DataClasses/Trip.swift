//
//  Trip.swift
//  TravelPal
//
//  Created by Matt Bommer on 10/27/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import Foundation
import UIKit
import FirebaseFirestore

class Trip {

    let uniqueId:String;
    var tripPicture:UIImage;
    var tripLocation:String;
    var tripDescription:String;
    var tripName:String;
    var startDate:Date;
    var endDate:Date;
    var startAge:Int;
    var endAge:Int;
    var travelers:[String];
    var likers:[String];
    var liked:Bool;
    var genderPref:String;
    var visible:Bool;
    var likeCount:Int;
    var imageData:Data;
    var long:Double;
    var lat:Double;
    
    init(userTrip: UIViewController) {
        let trip = userTrip as! CreateTripViewController
        
        var pref = ""
        switch trip.genderPref.selectedSegmentIndex {
        case 0:
            pref = "male"
        case 1:
            pref = "female"
        case 2:
            pref = "any"
        default:
            //This should never occur.
            print("error")
        }
        
        // Set the trips data from the text fields of the CreateTripVC
        self.tripLocation = trip.tripLocation
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        self.startDate = dateFormatter.date(from: trip.startDate.text!)!
        self.endDate = dateFormatter.date(from: trip.endDate.text!)!
        
        self.startAge = Int(trip.startAge.text!) ?? 0
        self.endAge = Int(trip.endAge.text!) ?? 65 // TODO fix this
        self.genderPref = pref
        self.tripPicture = trip.tripImage.image!
        self.visible = true
        self.likeCount = 0
        self.uniqueId = UUID().uuidString;
        self.imageData = self.tripPicture.pngData()!
        self.travelers = trip.travelers
        self.long = trip.long
        self.lat = trip.lat
        self.tripName = trip.tripName.text!
        self.tripDescription = trip.tripDescription.text!
        self.liked = false
        self.likers = []
        appUser.createdTrips.append(self.uniqueId)
    }
    
    // Initializer for when all trip data already exists (when pulling it from the database)
    init(tripName: String, tripLocation:String, tripDescription: String, startDate: Date, endDate:Date, startAge:Int, endAge:Int, genderPref:String, visible:Bool, uniqueId:String, travelers:[String], longitude: Double, latitude: Double, likers:[String]) {
        self.tripLocation = tripLocation
        self.startDate = startDate
        self.endDate = endDate
        self.startAge = startAge
        self.endAge = endAge
        self.genderPref = genderPref
        self.visible = visible
        self.likeCount = likers.count
        self.uniqueId = uniqueId
        self.tripPicture = UIImage()
        self.imageData = Data()
        self.travelers = travelers
        self.long = longitude
        self.lat = latitude
        self.tripName = tripName
        self.tripDescription = tripDescription
        self.likers = likers
        self.liked = likers.contains(appUser.email) ? true : false

        // Create a refrence to the pictures container
        let pathReference = storage.reference(withPath: "tripImages/\(uniqueId).png")
                // Pull trips picture down from the firestone storage
                pathReference.getData(maxSize: 20 * 1024 * 1024) { data, error in
                  if let error = error {
                    print("an error occured while trying to download the picture: \(error)")
                    self.tripPicture = UIImage(named: "logo")!
                    self.imageData = self.tripPicture.pngData()!
                  } else {
                    self.tripPicture = UIImage(data: data!)!
                    self.imageData = self.tripPicture.pngData()!
                  }
                }
    }
    
    // String format for puttin trip data into the database
    func dataFormat () -> [String: Any] {
            let tripData: [String: Any] = [
              "tripName": tripName,
              "tripDescription": tripDescription,
              "location": tripLocation,
              "startDate": startDate,
              "endDate": endDate,
              "startAge": startAge,
              "endAge": endAge,
              "visible": visible,
              "genderPref": genderPref,
              "travelers" : travelers,
              "lat" : lat,
              "long" : long,
              "likers": likers
            ]
          return tripData
          }
    
    func storeTripInDatabase () {
        // Spin up an instance of our trips collection
        db.collection("trips").document(self.uniqueId).setData(self.dataFormat())
        { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
            }
        }
        // Store our trip picture in our storage bucket and name the picture after the trips unique Id for easy finding
        let storageRef = storage.reference()
        let tripRef = storageRef.child("tripImages/\(self.uniqueId).png")
        let upload = tripRef.putData(self.imageData, metadata: nil) {
            (metadata, error) in guard let metadata = metadata else {
                print("An error occurred while uploading a picture \(error)")
                return
            }
        }
    }
    
    // Fucntion that increments the like count for a trip and adds it to the database.
    func incrementLike() {
        self.likeCount += 1
        self.liked = true
        self.likers.append(appUser.email)
        db.collection("trips").document(uniqueId).updateData([
            "likers": self.likers
        ]) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfuly written!")
            }
        }
    }

}
