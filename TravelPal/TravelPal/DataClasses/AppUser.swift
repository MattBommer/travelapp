//
//  AppUser.swift
//  TravelPal
//
//  Created by Kim Ha on 10/28/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit
import Firebase

// Class that stores information about a user/profile
class AppUser: NSObject {
    // For example:
    // Profile image
    // Saved trips
    // Name and email
    var name: String
    var age: String
    var location: String
    var dest0: String
    var dest1: String
    var dest2: String
    var bio: String
    var email: String
    var phoneNumber: String
    var ageMin: String
    var ageMax: String
    var trips:[Trip]
    var tripIds:[String]
    var popularTrips:[Trip]
    var profilePicture:UIImage
    var tripsSet:Bool
    var chatList:[String]
    var blockedList:[String]
    var createdTrips: [String]
    
    init(_ signupEmail: String){
        email = signupEmail
        phoneNumber = "(000) 000 - 000"
        name = "Default Name"
        age = "18"
        ageMin = "18"
        ageMax = "65+"
        location = "Default Location"
        dest0 = "Los Angeles, California"
        dest1 = "New York City, New York"
        dest2 = "Austin, Texas"
        bio = "Say something about yourself"
        trips = []
        tripIds = []
        popularTrips = []
        profilePicture = UIImage(named: "defaultProfile")!
        tripsSet = true
        chatList = []
        blockedList = []
        createdTrips = []
    }
    
    
    init(_ doc: [String:Any]) {
        email = doc["email"] as? String ?? ""
        phoneNumber = doc["phoneNumber"] as? String ?? ""
        name = doc["name"] as? String ?? ""
        age = doc["age"] as? String ?? ""
        ageMin = doc["ageMin"] as? String ?? ""
        ageMax = doc["ageMax"] as? String ?? ""
        location = doc["location"] as? String ?? ""
        dest0 = doc["dest0"] as? String ?? ""
        dest1 = doc["dest1"] as? String ?? ""
        dest2 = doc["dest2"] as? String ?? ""
        bio = doc["bio"] as? String ?? ""
        tripIds = doc["tripIds"] as? [String] ?? []
        chatList = doc["chatList"] as? [String] ?? []
        trips = []
        profilePicture = UIImage(named: "defaultProfile")!
        tripsSet = false
        popularTrips = []
        blockedList = doc["blockedList"] as? [String] ?? []
        createdTrips = []
    }
    
    func tripListener() {
        db.collection("trips").addSnapshotListener { querySnapshot, error in
            guard let snapshot = querySnapshot else {
                print("The following error occurred: \(error!)")
                return
            }
            snapshot.documentChanges.forEach { snap in
                if snap.type == .added {
                    let userData = snap.document.data()
                    
                    let trip = self.makeTrip(userData: userData, document: snap.document)
                    if !appUser.createdTrips.contains(trip.uniqueId) {
                        if appUser.tripIds.contains(trip.uniqueId){
                            self.addTrip(trip: trip, addId: false)
                        }else {
                            self.popularTrips.append(trip)
                        }
                    }
                }
            }
        }
    }
    
    
    func addTrip(trip:Trip, addId:Bool = true, wasPopular:Bool = false) {
        self.trips.append(trip)
        if wasPopular {
            for (n, t) in self.popularTrips.enumerated() {
                if t.uniqueId == trip.uniqueId {
                    self.popularTrips.remove(at: n)
                    break
                }
            }
        }
        if addId {
            self.tripIds.append(trip.uniqueId)
            db.collection("users").document(self.email).updateData([
                "tripIds": self.tripIds
            ]) { err in
                if let err = err {
                    print("Error writing document: \(err)")
                } else {
                    print("Document successfuly written!")
                }
            }
        }
    }
    
    // Remove trip by index
    func removeTrip(index: Int) {
        for (n, x) in tripIds.enumerated() {
            if x == trips[index].uniqueId{
                self.tripIds.remove(at: n)
                break
            }
        }
        self.popularTrips.append(self.trips.remove(at: index))
        
        db.collection("users").document(self.email).updateData([
            "tripIds": self.tripIds
        ]) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfuly written!")
            }
        }
    }
    
    func makeTrip(userData: [String: Any], document: QueryDocumentSnapshot) -> Trip {
        let startDate:Timestamp = userData["startDate"] as? Timestamp ?? Timestamp()
        let endDate:Timestamp = userData["endDate"] as? Timestamp ?? Timestamp()
        let trip = Trip(
            tripName: userData["tripName"] as? String ?? "",
            tripLocation: userData["location"] as? String ?? "",
            tripDescription: userData["tripDescription"] as? String ?? "",
            startDate: startDate.dateValue(),
            endDate: endDate.dateValue(),
            startAge:userData["startAge"] as? Int ?? 18,
            endAge:userData["endAge"] as? Int ?? 18,
            genderPref:userData["genderPref"] as? String ?? "",
            visible:userData["visible"] as? Bool ?? true,
            uniqueId: document.documentID,
            travelers: userData["travelers"] as? [String] ?? [String](),
            longitude: userData["long"] as? Double ?? 0.0,
            latitude: userData["lat"] as? Double ?? 0.0,
            likers: userData["likers"] as? [String] ?? []
        )
        return trip
    }
    
    func sourceTrips() {
        db.collection("trips").getDocuments { (tripQuerySnap, error) in
            if let error = error {
                print("Error: \(error)")
                return
            } else {
                for document in tripQuerySnap!.documents{
                    let userData = document.data()
                    let trip = self.makeTrip(userData: userData, document: document)
                    if self.tripIds.contains(document.documentID){
                        appUser.addTrip(trip: trip, addId: false)
                    }else if trip.visible {
                        appUser.popularTrips.append(trip)
                    }
                }
            }
        }
        self.tripsSet = true
    }
    
    // Add more params as we need them
    func dbFormat() -> [String:Any] {
        let docData: [String: Any] = [
          "email": email,
          "phoneNumber": phoneNumber,
          "name": name,
          "age": age,
          "ageMin": ageMin,
          "ageMax": ageMax,
          "location": location,
          "dest0": dest0,
          "dest1": dest1,
          "dest2": dest2,
          "bio": bio,
          "tripIds": tripIds,
          "chatList": chatList
        ]
      return docData
      }
}
