//
//  SearchTableViewCell.swift
//  TravelPal
//
//  Created by Matt Bommer on 11/14/20.
//  Copyright © 2020 CS371L-Team2. All rights reserved.
//

import UIKit
import MapKit

class SearchTableViewCell: UITableViewCell {
    
    static let identifier = "searchCell"
    
    let name : UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)
        return label
    }()
    
    let address : UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.textAlignment = .left
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.subheadline)
        return label
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(name)
        contentView.addSubview(address)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        name.frame = CGRect (x: 5,
                             y: 5,
                             width: contentView.frame.size.width,
                             height: 20)
        address.frame = CGRect (x: 5,
                                y: 5 + name.frame.height,
                             width: contentView.frame.size.width,
                             height: 20)
        
    }
}
